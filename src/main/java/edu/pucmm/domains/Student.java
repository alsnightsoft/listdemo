package edu.pucmm.domains;

public class Student {

    private long id;
    private String names;
    private String lastnames;

    public Student() {
    }

    public Student(long id, String names, String lastnames) {
        this.id = id;
        this.names = names;
        this.lastnames = lastnames;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getLastnames() {
        return lastnames;
    }

    public void setLastnames(String lastnames) {
        this.lastnames = lastnames;
    }

    @Override
    public String toString() {
        return "ID: " + id + "\nEstudiante: " + names + " " + lastnames;
    }
}
