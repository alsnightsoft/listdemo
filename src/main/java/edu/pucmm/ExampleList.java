package edu.pucmm;

import edu.pucmm.domains.Student;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ExampleList {

    public static void main(String[] args) {
        ExampleList exampleList = new ExampleList();
        exampleList.demoStringList();
        exampleList.demoStudentList();
    }

    private void demoStringList() {
        List<String> names = new ArrayList<>();
        names.add("Aluis");
        names.add("Jose");
        names.add("Pedro");
        names.add("May");
        names.add("June");
        names.add("Candido");
        names.add("Reymon");
        System.out.println("Imprimo mi lista sin eliminar");
        printList(names);
        List<String> toDelete = new ArrayList<>();
        for (String name : names) {
            if (name.toLowerCase().contains("a")) {
                toDelete.add(name);
            }
        }
        names.removeAll(toDelete);
        System.out.println("Lista con todos los nombres con A eliminados");
        printList(names);
    }

    private void demoStudentList() {
        List<Student> students = new ArrayList<>();
        students.add(new Student(1L, "Aluis", "Marte"));
        students.add(new Student(2L, "Jose", "Peralta"));
        students.add(new Student(56L, "Pedro", "Gonzales"));
        students.add(new Student(3L, "May", "Perez"));
        students.add(new Student(5L, "June", "Martinez"));
        students.add(new Student(77L, "Candido", "Hernandez"));
        students.add(new Student(100L, "Reymon", "Garcia"));
        System.out.println("Imprimo mi lista sin eliminar");
        printList(students);
        System.out.println("Ordeno en base al indice");
        sortAndPrint(students, new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                if (o1.getId() < o2.getId()) {
                    return 1;
                } else if (o1.getId() > o2.getId()) {
                    return -1;
                }
                return 0;
            }
        });
        System.out.println("Ordeno en base al Nombre");
        sortAndPrint(students, new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
               return o1.getNames().toLowerCase().compareTo(o2.getNames().toLowerCase());
            }
        });
        System.out.println("Ordeno en base al Apellido");
        sortAndPrint(students, new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return o1.getLastnames().toLowerCase().compareTo(o2.getLastnames().toLowerCase());
            }
        });
        List<Student> toDelete = new ArrayList<>();
        for (Student student : students) {
            if (student.getNames().toLowerCase().contains("a")) {
                toDelete.add(student);
            }
        }
        students.removeAll(toDelete);
        System.out.println("Lista con todos los estudiantes con A eliminados");
        printList(students);
        // Si la lista esta ordenada puedo buscar y eliminar de forma más rápida y esto lo deben tomar en cuenta.
    }

    private void sortAndPrint(List<Student> students, Comparator<Student> comparator) {
        students.sort(comparator);
        printList(students);
    }

    private void printList(List list) {
        for (Object o : list) {
            System.out.println(o.toString());
        }
    }
}
